"""create table terminal

Revision ID: b43438ae0d1d
Revises: 
Create Date: 2023-02-07 10:51:47.608153

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b43438ae0d1d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'terminal',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('provinsi', sa.String(50), nullable=False),
        sa.Column('kabupaten', sa.String(200)),
        sa.Column('tipe_terminal', sa.String(50)),
        sa.Column('jumlah', sa.Integer()),
        sa.Column('satuan', sa.String(10)),
        sa.Column('tahun', sa.String(10)),
    )


def downgrade() -> None:
    op.drop_table('terminal')
