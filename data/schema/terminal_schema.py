from pydantic import BaseModel


class Terminal(BaseModel):
    id: int
    provinsi: str
    kabupaten: str
    tipe_terminal: str
    jumlah: int
    satuan: str
    tahun: str


class TerminalByKabupaten(BaseModel):
    jumlah: int
    kabupaten: str


class TerminalByTahun(BaseModel):
    jumlah: int
    kabupaten: str
    tahun: str


class TerminalByTipe(BaseModel):
    jumlah: int
    kabupaten: str
    tahun: str
    tipe: str

class TerminalRecap(BaseModel):
    tipe_terminal: str
    jumlah: str