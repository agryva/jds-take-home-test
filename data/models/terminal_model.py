from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Terminal(Base):
    __tablename__ = 'terminal'
    id = Column(Integer, primary_key=True, index=True)
    provinsi = Column(String)
    kabupaten = Column(String)
    tipe_terminal = Column(String)
    jumlah = Column(Integer)
    satuan = Column(String)
    tahun = Column(String)
