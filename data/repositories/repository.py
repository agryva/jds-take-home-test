from fastapi_sqlalchemy import db
from sqlalchemy import text
from data.models.terminal_model import Terminal as TerminalModel


class Repository:
    def getDataTerminal():
        return db.session.query(TerminalModel).all()

    def getDataTerminalGroupByKabupaten(kabupaten: str | None = None):
        list = []

        if kabupaten is not None:
            result = db.session.execute(text(
                f"select sum(terminal.jumlah), terminal.kabupaten from terminal where terminal.kabupaten LIKE '%{kabupaten.upper()}%' group by terminal.kabupaten")).fetchall()
        else:
            result = db.session.execute(text(
                'select sum(terminal.jumlah), terminal.kabupaten from terminal group by terminal.kabupaten')).fetchall()

        for row in result:
            list.append({
                'jumlah': row[0],
                'kabupaten': row[1]
            })
        return list

    def getDataTerminalGroupByKabupatenPerTahun(tahun: str | None = None):
        if tahun is not None:
            result = db.session.execute(text(
                f"select sum(terminal.jumlah), terminal.kabupaten, terminal.tahun from terminal where terminal.tahun = '{tahun}' group by terminal.kabupaten, terminal.tahun order by tahun ASC")).fetchall()
        else:
            result = db.session.execute(text(
                f"select sum(terminal.jumlah), terminal.kabupaten, terminal.tahun from terminal group by terminal.kabupaten, terminal.tahun order by tahun ASC")).fetchall()
        list = []

        for row in result:
            list.append({
                'jumlah': row[0],
                'kabupaten': row[1],
                'tahun': row[2]
            })
        return list

    def getDataTerminalByRawQuery(sql: str):
        result = db.session.execute(text(sql)).fetchall()

        return result

    def insertData(terminal: TerminalModel):
        print(terminal)
        db.session.add(terminal)
        db.session.commit()
        return True
