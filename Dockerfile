FROM python:3.10.1-alpine

# ARG DATABASE_URL
# ENV DATABASE_URL $DATABASE_URL
# ARG TOKEN
# ENV TOKEN $TOKEN

WORKDIR /app
COPY requirements.txt .
RUN pip install --upgrade pip
RUN \
    apk add --no-cache postgresql-libs && \
    apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
    apk --purge del .build-deps

RUN apk --update add build-base freetype-dev libpng-dev openblas-dev
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

# RUN alembic upgrade head

EXPOSE 5001
CMD [ "python3", "main.py" ]
