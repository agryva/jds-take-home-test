from data.repositories.repository import Repository
from fastapi_sqlalchemy import db
from data.schema.terminal_schema import Terminal as TerminalSchema
from data.models.terminal_model import Terminal as TerminalModel
from data.repositories.repository import Repository


class TerminalUseCase():
    def insertData(terminal: TerminalSchema):
        db_terminal = TerminalModel(
            id=terminal.id,
            provinsi=terminal.provinsi,
            kabupaten=terminal.kabupaten,
            tipe_terminal=terminal.tipe_terminal,
            jumlah=terminal.jumlah,
            satuan=terminal.satuan,
            tahun=terminal.tahun
        )
        return Repository.insertData(db_terminal)

    def getAllData():
        return Repository.getDataTerminal()

    def getDataByKabupaten(kabupaten: str | None = None):
        return Repository.getDataTerminalGroupByKabupaten(kabupaten=kabupaten)

    def getDataByKabupatenPerTahun(tahun: str | None = None):
        return Repository.getDataTerminalGroupByKabupatenPerTahun(tahun=tahun)

    def getDataByTipe(tipe: str = '', tahun: str | None = None):
        if tahun is not None:
            response = Repository.getDataTerminalByRawQuery(
                f"select terminal.kabupaten, terminal.jumlah, terminal.tipe_terminal, terminal.tahun from terminal where terminal.tahun = '{tahun}' AND terminal.tipe_terminal = '{tipe}' order by terminal.tipe_terminal ASc")
        else:
            response = Repository.getDataTerminalByRawQuery(
                f"select terminal.kabupaten, terminal.jumlah, terminal.tipe_terminal, terminal.tahun from terminal where terminal.tipe_terminal = '{tipe}' order by terminal.tipe_terminal ASc")
        list = []

        for row in response:
            list.append({
                'kabupaten': row[0],
                'jumlah': row[1],
                'tipe': row[2],
                'tahun': row[3]
            })

        return list

    def getDataRecapTipeTerminal():
        response = Repository.getDataTerminalByRawQuery(
            'select sum(terminal.jumlah) AS jumlah, terminal.tipe_terminal from terminal group by terminal.tipe_terminal'
        )
        list = []

        for row in response:
            list.append({
                'jumlah': row[0],
                'tipe': row[1],
            })

        return list
