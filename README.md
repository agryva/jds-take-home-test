<h1 align="center">
	JDS TAKE HOME TEST 🚀
</h1>

## 🚀 Information

This application has been deployed on public IP http://194.233.77.215:5001/ and to see the API documentation, you can go to the [**Api documentation**](http://194.233.77.215:5001/docs)

#### Tech Stack

- [**Python**](https://www.python.org/) Programming language with version 3.10 above
- [**Postgresql**](https://www.postgresql.org/) Object-relational database

#### Depedencies

- [**Uvicorn**](https://pypi.org/project/uvicorn) Uvicorn is an ASGI web server implementation for Python.
- [**Fastapi**](https://pypi.org/project/fastapi/) Web framework for building APIs with Python 3.7+ based on standard Python type hints.
- [**Pandas**](https://pypi.org/project/pandas/) Library for read excel file
- [**Dotenv**](https://pypi.org/project/dotenv/) Library for read environment file

### Instalation
* Download & Install Docker to your machine

* Build application using docker compose

	docker-compose up -d --build
