
import os
from fastapi import Header, HTTPException


async def verify_token(token: str = Header()):
    if token != os.getenv('TOKEN'):
        raise HTTPException(status_code=400, detail="Token header invalid")
