from fastapi import APIRouter, Depends

from data.schema.terminal_schema import TerminalByKabupaten as TerminalByKabupatenSchema, TerminalByTahun as TerminalByTahunSchema, TerminalByTipe as TerminalByTipeSchema
from domain.terminal.terminal_usecase import TerminalUseCase
from utils.auth_middleware import verify_token

router = APIRouter(
    prefix="/terminal",
    tags=["terminal"]
)


@router.get('/', response_model=list)
def terminal():
    response = TerminalUseCase.getAllData()
    return response


@router.get('/kabupaten', dependencies=[Depends(verify_token)], response_model=list[TerminalByKabupatenSchema])
def read_by_kabupaten(kabupaten: str | None = None):
    response = TerminalUseCase.getDataByKabupaten(kabupaten)
    return response


@router.get('/tahun', dependencies=[Depends(verify_token)], response_model=list[TerminalByTahunSchema])
def read_by_kabupaten(tahun: str | None = None):
    response = TerminalUseCase.getDataByKabupatenPerTahun(tahun=tahun)
    return response

@router.get('/tipe', dependencies=[Depends(verify_token)], response_model=list[TerminalByTipeSchema])
def read_by_kabupaten(tipe: str, tahun: str | None = None):
    response = TerminalUseCase.getDataByTipe(tahun=tahun, tipe=tipe)
    return response


@router.get('/tipe/recap', dependencies=[Depends(verify_token)])
def read_by_kabupaten():
    response = TerminalUseCase.getDataRecapTipeTerminal()
    return response
