# IMPORT FROM SYSTEM
import os
import uvicorn

# IMPORT FROM LIBRARY
from openpyxl import load_workbook
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, Request, Response
from fastapi_sqlalchemy import DBSessionMiddleware
from dotenv import load_dotenv

# IMPORT FROM PROJECT
from domain.terminal.terminal_usecase import TerminalUseCase
from data.schema.terminal_schema import Terminal as TerminalSchema
from presentation import terminal_presentation
load_dotenv('.env')


app = FastAPI(
    title='JDS TECH TEST',
    version='1.0.0'
)

app.add_middleware(DBSessionMiddleware, db_url=os.environ['DATABASE_URL'])
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/')
def read():
    return {'data': 'API JDS TECH TEST'}


@app.get('/import')
def importData():
    dataSets = []
    workbook = load_workbook(filename="./dataset.xlsx")
    worksheet = workbook.worksheets[0]
    index = 0
    for r in worksheet.rows:
        column = [cell.value for cell in r]
        if index > 0:
            dataSets.append(TerminalSchema(
                id=(column[0]),
                provinsi=column[2],
                kabupaten=column[4],
                tipe_terminal=column[5],
                jumlah=(column[6]),
                satuan=column[7],
                tahun=column[8]
            ))
        index += 1
    # dataframe1 = pd.read_excel('dataset.xlsx')
    # for _, row in dataframe1.iterrows():
    #     dataSets.append(TerminalSchema(
    #         id=int(row[0]),
    #         provinsi=row[2],
    #         kabupaten=row[4],
    #         tipe_terminal=row[5],
    #         jumlah=int(row[6]),
    #         satuan=row[7],
    #         tahun=row[8]
    #     ))
    for data in dataSets:
        TerminalUseCase.insertData(data)
    return {'data': dataSets}


app.include_router(terminal_presentation.router)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=int(os.getenv('APP_PORT')))
